# GranuHeap Digital Twin

![GranuHeap-DigitaTwin-Render](readme_images/GH_image.png){center}

## Description
A discrete element method (DEM) Digital Twin of the Granutools' GranuHeap powder characterisation tool built using PICI-LIGGGHTS.
This digital twin allows users to understand in more depth the behaviour of a powder in the GranuHeap. 
Particle properties can be defined and the individual particle data or bulk powder behaviour extracted and analysed.

## Prerequisites
To be able to use this digital twin an installation of PICI-LIGGGHTS is required. A step-by-step guide of how to install PICI-LIGGGHTS can be found [here](https://uob-positron-imaging-centre.github.io/InstallingPICI-LIGGGHTS/).

## Getting Started
Using the digital twin is very simple but to explore the behaviour of powder in the GranuHeap digital twin the particle properties are required. 
Most of the particle properties can be easily defined by changing the values in the parameters.txt file.
The parameters.txt file allows the sliding friction, rolling friction, restitution, material density, cohesive energy density and number of particles to be defined.
The particle size distribution also needs to be defined. This is currently done by going into the granuheap_liggghts.sim file and manually changing it.
Previous knowledge of LIGGGHTS is highly recommended to do this currently.

Once PICI-LIGGGHTS has been installed and the particle properties have been defined the digital twin can be used by running the 'granuheap.py'.

## Post-Processing

The GranuHeap digital twin outputs NumPy files with the particle positions and radii. These files can be used to calculate the angle of repose by using the 'compute_angle_of_repose.py' file
in the 'Post Processing' folder. An example data folder has been provided with the NumPy files from a previous simulation. By setting the 'data_path' variable in 'compute_angle_of_repose.py'
to the folder containing the NumPy files the angle of repose can be calculated. The angle is calculated in the same way as the real GranuHeap.

The graph below shows an example of results that have been obtained in the GranuHeap digital twin. The angle of repose was measured for a granular material with sliding friction
values between 0.15 and 1.0 and rolling friction values between 0 and 0.7.

<div style="width:350px; margin:0 auto;">
<img src="readme_images/ar_vs_sf.png" alt="Graph of example angle of repose data made in the GranuHeap Digital Twin">
</div>

## Roadmap
- [x] Add parameter text input file.
- [ ] Develop way of automatically inputting particle size distribution.
- [x] Include finalised post-processing script to calculate angle of repose.

## Issues
If you run into any issues or bugs let us know by raising and issue on the GitLab! If you find a way to fix it raise a pull request!

## Authors and acknowledgment
The development of GranuTools digital-twins was made during the PhD thesis of Ben Jenkins in the framework 
of a collaboration between GranuTools and University of Birmingham under the academic supervision of 
Dr. Kit Windows-Yule and Prof. Jonathan Seville.

## Project status
Actively being worked on!
