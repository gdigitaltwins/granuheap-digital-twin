#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# File   : compute_repose.py
# License: GNU v3.0
# Author : Andrei Leonard Nicusan <a.l.nicusan@bham.ac.uk>
# Date   : 29.11.2022

import textwrap

import numpy as np
import konigcell as kc

from tqdm import tqdm


class ReposeTester:
    '''Simulate a static angle of repose tester based on the particles
    placed on a heap (from a DEM simulation or otherwise).
    '''

    def __init__(self, positions, radii, base):

        # Input type-checking
        positions = np.asarray(positions, dtype = float)
        if positions.ndim != 2 or positions.shape[1] != 3:
            raise ValueError(textwrap.fill((
                "The input `positions` must be a 2D array with shape (P, 3), "
                "with columns representing the X, Y, Z coordinates of P "
                f"particles. Received array with shape `{positions.shape}`."
            )))

        # Radii can be a single number (i.e. non-iterable) or vector
        if not hasattr(radii, "__iter__"):
            radii = radii * np.ones(len(positions))
        else:
            radii = np.asarray(radii, dtype = float)

        if radii.ndim != 1 or len(radii) != len(positions):
            raise ValueError(textwrap.fill((
                "The input `radii`, if given as a vector, must have the same "
                "length as the number of positions given. Received "
                f"{len(radii)} radii for {len(positions)} particles."
            )))

        self.positions = positions
        self.radii = radii
        self.base = float(base)


    def angles_of_repose(self, angles):
        '''Compute angles of repose by rotating particles by `angles` radians
        around the Z axis and simulating photographing the particles.

        The input `angles` can be given as either:

        - A single integer representing the number of photos around the
          particles to take (i.e. number of angles between 0 and 2pi).
        - A vector of explicit angles to photograph the heap at.
        '''

        # Angles can either be an explicit vector of angles or a number of
        # increments between 0 and 2pi
        if not hasattr(angles, "__iter__"):
            angles = np.linspace(0, 2 * np.pi, int(angles), endpoint = False)
        else:
            angles = np.asarray(angles, dtype = float)
            if angles.ndim != 1:
                raise ValueError(textwrap.fill((
                    "The input `angles`, if defined as an explicit list of "
                    "angles to image, must be a 1D vector of angles around "
                    "the XZ plane of the particles given in radians. Received "
                    f"`{angles}`."
                )))

        # For each angle around Z, project particles onto 2D plane and
        # rasterize them using KonigCell
        reposes = []
        for angle in tqdm(angles, desc = "Angle :"):

            # Take "photo" of heap at `angle` radians around Z
            pixels = self.photo(angle)

            # Compute projected particles' area as PixelArea * NumNonZeroPixels
            area = np.prod(pixels.pixel_size) * np.sum(pixels.pixels != 0.)

            # Compute base angle of isoscelles triangle with same area
            aor = np.arctan(4 * area / self.base / self.base)
            reposes.append(np.rad2deg(aor))

        return np.array(reposes)


    def project(self, angle):
        '''Project particles onto plane rotated `angle` radians around Z, such
        that angle 0 is towards +Y; the returned planar positions have two
        columns [X, Z].
        '''

        rotation = np.array([
            [np.cos(angle), -np.sin(angle), 0],
            [np.sin(angle), np.cos(angle), 0],
            [0, 0, 1],
        ])

        return np.dot(rotation, self.positions.T).T[:, [0, 2]]


    def photo(self, angle):
        '''Return pixellised image of particles projected `angle` radians
        around Z.
        '''

        # Pixellise projected particles
        projection = self.project(angle)
        pixels = kc.static2d(
            projection,
            kc.ONE,
            radii = self.radii,
            resolution = (1024, 1024),
            verbose = False,
        )
        return pixels


    def plot(self, angle = 0):
        '''Project, photograph and return a Plotly figure of the 2D heap.
        '''

        # Show a Plotly heatmap of the pixels
        pixels = self.photo(angle)

        fig = kc.create_fig()
        fig.add_trace(pixels.heatmap_trace())
        fig.update_layout(
            xaxis_title_text = "X",
            yaxis_title_text = "Z",
            yaxis_scaleanchor = "x",
            yaxis_scaleratio = 1,
        )

        return fig


    def __repr__(self):
        # Pretty-print all runtime-discovered attributes set
        name = self.__class__.__name__
        underline = "-" * len(name)

        # Select all attributes from dir(obj) that are not callable (i.e. not
        # methods) and don't start with "_" (i.e. they're private)
        docs = []
        for att in dir(self):
            memb = getattr(self, att)
            if not callable(memb) and not att.startswith("_"):
                memb_str = str(memb)
                if "\n" in memb_str:
                    memb_str = memb_str.partition("\n")[0] + "..."
                docs.append(f"{att} = {memb_str}")

        return f"{name}\n{underline}\n" + "\n".join(docs)


# Load positions and radii, define repose tester base length
data_path = 'Example Data'
positions = np.load(f"{data_path}/positions.npy")
radii = np.load(f"{data_path}/radii.npy")
base_length = 0.04

# Create ReposeTester class
tester = ReposeTester(positions, radii, base_length)
print(tester)

# Simplest usage - get angles of repose at 16 angles around the heap
angles = tester.angles_of_repose(16)
print(np.mean(angles))

# Or explicitly define what angles to compute angle of repose at
angles = tester.angles_of_repose([0, np.pi / 4, np.pi / 2, np.pi])

# Compute 2D projection of particle positions at `angle` radians around Z+
projections = tester.project(angle = np.pi / 2)

# Take "photo" / "X-Ray" of particles at given `angle`
pixels = tester.photo(angle = np.pi / 2)

# Create Plotly figure of particles' photo at a given `angle`
fig = tester.plot(angle = np.pi / 2)
fig.show()
