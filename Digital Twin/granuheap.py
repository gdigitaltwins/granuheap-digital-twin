#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# File   : granuheap.py
# License: GNU v3.0


import os
import numpy as np
import coexist
import plotly.graph_objs as go
import pandas as pd


# Directory to save results to
results_dir = "results"
if not os.path.isdir(results_dir):
    os.mkdir(results_dir)

# Relevant paths
script_path = "granuheap_liggghts.sim"
parameters_path = 'parameters.txt'

# Extract parameters from parameters file and update liggghts script
df = pd.read_csv(parameters_path, sep='=')
nparticles = df.loc['number_of_particles']['DEM Particle Parameters']
sliding = df.loc['sliding_friction']['DEM Particle Parameters']
rolling = df.loc['rolling_friction']['DEM Particle Parameters']
restitution = df.loc['restitution']['DEM Particle Parameters']
cohesion = df.loc['cohesive_energy_density']['DEM Particle Parameters']
density = df.loc['density']['DEM Particle Parameters']

# Update liggghts script
with open(script_path, "r") as f:
    sim_script = f.readlines()

# Simulation log path
sim_script[1] = f"log {results_dir}/granuheap.log\n"
sim_script[10] = f"variable N equal {nparticles}\n"

# Parameter naming:
#    PP  = Particle-Particle
#    PW  = Particle-Wall
#    PSW = Particle-Sidewall
sim_script[22] = f"variable fricPP equal {sliding}\n"
sim_script[23] = f"variable fricPW equal {sliding}\n"
sim_script[24] = f"variable fricPSW equal {sliding}\n"

sim_script[27] = f"variable fricRollPP equal {rolling}\n"
sim_script[28] = f"variable fricRollPW equal {rolling}\n"
sim_script[29] = f"variable fricRollPSW equal {rolling}\n"

sim_script[32] = f"variable corPP equal {restitution}\n"
sim_script[33] = f"variable corPW equal {restitution}\n"
sim_script[34] = f"variable corPSW equal {restitution}\n"

sim_script[37] = f"variable cohPP equal {cohesion}\n"
sim_script[38] = f"variable cohPW equal {cohesion}\n"
sim_script[39] = f"variable cohPSW equal {cohesion}\n"

sim_script[42] = f"variable dens equal {density}\n"


# Save the simulation template with the modified parameters
sim_path = f"{results_dir}/granuheap.sim"
with open(sim_path, "w") as f:
    f.writelines(sim_script)


# Load modified simulation script
sim = coexist.LiggghtsSimulation(sim_path, verbose=True)


# Run simulation up to given time (s)
line = "\n" + "-" * 80 + "\n"

print(line + "Pouring particles and letting them settle" + line)
sim.step_time(2.0)

print(line + "Lifting GranuHeap wall and letting particles settle" + line)
sim.execute_command("fix move all move/mesh mesh wall linear 0.0 0.0 0.005")

sim.step_time(4.0)


# Extract particle properties as NumPy arrays
time = sim.time()
radii = sim.radii()
positions = sim.positions()
velocities = sim.velocities()

print("\n\n" + line)
print(f"Simulation time: {time} s\nParticle positions:\n{positions}")
print(line + "\n\n")


# Save results as efficient binary NPY-formatted files
np.save(f"{results_dir}/radii.npy", radii)
np.save(f"{results_dir}/positions.npy", positions)
np.save(f"{results_dir}/velocities.npy", velocities)
